# Behavior Labels

Behavioral annotations using the malware behavioral catalog as a taxonomy. The 
behaviors contain an overall objective and the techniques employed to meet that 
objective.

## Contents:
* `microsoftdata_behaviors.xlsx`: The original labeling results as contained in the paper
* `MALGEN_LABELS.csv`: Updated with multiple passes over the labeling
* `MBC_Labels.xlsx`: Justification for the derived behavioral labeling for each malware
family. Contains excerpts from threat reports to validate the inclusion of a given
behavior as defined by MBC.

## Funding Statement
Sandia National Laboratories is a multimission laboratory managed and operated 
by National Technology \& Engineering Solutions of Sandia, LLC, a wholly owned 
subsidiary of Honeywell International Inc., for the U.S. Department of 
Energy's National Nuclear Security Administration under contract DE-NA0003525. 

* SAND2020-5090 O  - microsoftdata_behaviors.xlsx
* SAND2020-12359 O - MBC_Labels.xlsx and MALGEN_LABELS.csv
