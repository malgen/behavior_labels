################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

from torch.utils.data.sampler import Sampler as Sampler
from torch.utils.data import Dataset
import numpy as np
import math
from malgen.data_processing.gather_data import \
    gather_all_parts,\
    generate_image
from torchvision.transforms import ToTensor


class MicrosoftMalwareImages(Dataset):
    """
    This class represents a dataset of microsoft malware binaries
    as images
    """
    def __init__(self, bytes_files, family_file, family_name_file,
                 behavior_file, image_shape, old=True):
        """
        Gathers all of our data together
        
        Parameters
        ----------
        bytes_files: list(str)
            A list of file locations for .bytes files to transform to images
        family_file: str
            File that maps filename to family label
        family_name_file: str
            File that maps family label to family name
        behavior_file: str
            File that maps family name to behavior vector    
        """
        self.file_details = gather_all_parts(bytes_files, family_file,
                                             family_name_file, behavior_file,
                                             old=old)
        self.image_shape = image_shape
        self.family_to_num_examples = self.examples_per_class()

    def __len__(self):
        # TODO we can have multiple images per file
        return len(self.file_details)

    def __getitem__(self, inputs):
        raise NotImplementedError("Base class, implement in children")

    def __get_behavior__(self, filename):
        filename_index = self.file_details['filename'] == filename
        behaviors = self.file_details[filename_index]['behaviors'].values[0]
        return behaviors

    def __get_image__(self, index, family=None):
        family_idx = self.file_details['family_number'] == family
        if family is None:
            file_candidates = self.file_details['filename']
        else:
            file_candidates = self.file_details['filename'][family_idx]
        image_filename = file_candidates.iloc[index]
        filename_index = self.file_details['filename'] == image_filename
        image_file = self.file_details[filename_index]['file_path'].values[0]
        image = generate_image(image_file, self.image_shape)
        return image, image_filename

    def examples_per_class(self):
        """
        Based on batch size determine how many full batches per family
        """
        return self.file_details['family_number'].value_counts()


class MicrosoftMalwareFamilyImages(MicrosoftMalwareImages):

    def __init__(self, bytes_files, family_file, family_name_file,
                 behavior_file, image_shape, binary_return=False,
                 batched=True, old=True):
        """
        Gathers all of our data together
        
        Parameters
        ----------
        bytes_files: list(str)
            A list of file locations for .bytes files to transform to images
        family_file: str
            File that maps filename to family label
        family_name_file: str
            File that maps family label to family name
        behavior_file: str
            File that maps family name to behavior vector    
        binary_return: bool
            Control whether to return family integer or one-hot encoding
        batched: bool
            Whether or not to return a batch of a singular family
        """
        self.binary_return = binary_return
        self.batched = batched
        super().__init__(bytes_files, family_file, family_name_file,
                         behavior_file, image_shape, old)

    def __getitem__(self, inputs):
        if self.batched:
            item, label = self.__get_single_family_batch__(inputs)
        else:
            item, label = self.__get_multi_family_batch__(inputs)
        return item, np.array(label)

    def __get_single_family_batch__(self, inputs):
        idx, family = inputs
        image, _ = self.__get_image__(idx, family)
        return_tensor = ToTensor()(image)
        family_vector = np.zeros((9,))
        # Family is 1 indexed
        family_vector[family-1] = 1
        if self.binary_return:
            return return_tensor, family_vector
        else:
            return return_tensor, family-1

    def __get_multi_family_batch__(self, inputs):
        idx = inputs
        image, image_filename = self.__get_image__(idx)
        return_tensor = ToTensor()(image)

        filename_index = self.file_details['filename'] == image_filename
        family = self.file_details['family_number'][filename_index].values
        family_vector = np.zeros((9,))
        family_vector[family-1] = 1

        if self.binary_return:
            return return_tensor, family_vector
        else:
            return return_tensor, family-1


class MicrosoftMalwareBehaviorImages(MicrosoftMalwareImages):

    def __init__(self, bytes_files, family_file, family_name_file,
                 behavior_file, image_shape, batched=True, old=True):
        """
        Gathers all of our data together
        
        Parameters
        ----------
        bytes_files: list(str)
            A list of file locations for .bytes files to transform to images
        family_file: str
            File that maps filename to family label
        family_name_file: str
            File that maps family label to family name
        behavior_file: str
            File that maps family name to behavior vector
        batched: bool
            Whether or not to return a batch of a singular family
        """
        self.batched = batched
        super().__init__(bytes_files, family_file, family_name_file,
                         behavior_file, image_shape, old)

    def __getitem__(self, inputs):
        if self.batched:
            item, label = self.__get_single_behavior_batch__(inputs)
        else:
            item, label = self.__get_multi_behavior_batch__(inputs)
        return item, np.array(label)

    def __get_single_behavior_batch__(self, inputs):
        idx, family = inputs
        image, image_filename = self.__get_image__(idx, family)
        return_tensor = ToTensor()(image)
        behaviors = self.__get_behavior__(image_filename)
        return return_tensor, behaviors

    def __get_multi_behavior_batch__(self, inputs):
        idx = inputs
        image, image_filename = self.__get_image__(idx)
        return_tensor = ToTensor()(image)
        behaviors = self.__get_behavior__(image_filename)
        return return_tensor, behaviors


class MalwareBatchSampler(Sampler):
    """
    This class will create batches of binary images from malware samples
    """
    def __init__(self, data_source, num_samples, shuffle=True, seed=123):
        self.local_random = np.random.RandomState(seed)
        self.shuffle = shuffle
        self.data_source = data_source
        self.num_samples = num_samples
        self.family = 0
        if not shuffle:
            self.family_index = dict(zip(np.arange(1, 9), [0]*9))

    def shuffled_generator(self, family_to_batches):
        batch_indices = []
        for family in family_to_batches.keys():
            number_of_batches = family_to_batches[family]
            family_batches = [family]*number_of_batches
            batch_indices.extend(family_batches)
        self.local_random.shuffle(batch_indices)

        for family in batch_indices:
            family_labels = self.data_source.file_details['family_number'].values
            fam_examples = family_labels == family
            num_family_examples = sum(fam_examples)
            # Yield treats the list as an item from an iterator
            number_samples = min(self.num_samples, num_family_examples)
            yield list(zip(self.local_random.choice(num_family_examples,
                                                    size=number_samples,
                                                    replace=False),
                           [family] * self.num_samples))

    def fixed_generator(self, family_to_batches):
        raise NotImplementedError("TODO")

    def __iter__(self):
        family_to_batches = self.data_source.family_to_num_examples.copy()
        for family in family_to_batches.keys():
            number_of_batches = math.ceil(family_to_batches[family]/self.num_samples)
            family_to_batches[family] = number_of_batches
        if self.shuffle:
            yield from self.shuffled_generator(family_to_batches)
        else:
            yield from self.fixed_generator(family_to_batches)

    def __len__(self):
        return self.num_samples
