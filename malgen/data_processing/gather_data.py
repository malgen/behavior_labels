################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

import pandas as pd
import os
import sys
from PIL import Image
import numpy as np
np.set_printoptions(threshold=sys.maxsize)


def gather_all_parts(bytes_files, family_file, family_name_file,
                     behavior_file, old=True):
    filepath_filename = gather_filepath_filename(bytes_files)
    filename_familynumber = gather_filename_familynumber(family_file)
    path_name_number = pd.merge(filename_familynumber, filepath_filename,
                                on="filename", how="inner")
    if old:
        familyname_behaviors = gather_familyname_behaviors(behavior_file)
    else:
        familyname_behaviors = gather_familyname_behaviors_new(behavior_file)
    familyname_familynumber = gather_familyname_familynumber(family_name_file)
    familyname_behaviors_number = \
        pd.merge(familyname_behaviors, familyname_familynumber,
                 on="family_name", how="inner")

    file_details = pd.merge(path_name_number, familyname_behaviors_number,
                            on="family_number", how="inner")
    return file_details


def gather_filepath_filename(file_paths):
    """

    Returns
    -------
    file_details: pd.DataFrame
        With columns, filename, file_path, family_num, family_name, behaviors
        We have data from a variety of sources so normalizing it in this way
        seemed most extensible.
    """
    file_details = pd.DataFrame()
    file_details['file_path'] = file_paths
    file_details['filename'] = file_details['file_path'].\
        apply(get_filename_from_path)
    return file_details


def get_filename_from_path(file_path):
    file_name = os.path.basename(file_path)
    file_name, _ = file_name.split(os.extsep, 1)
    return file_name


def gather_familyname_behaviors(file_name):
    """
    Helper to read our behavioral annotations
    Parameters
    ----------
    file_name: str
        Input file name that pairs families to behaviors
    """
    assert os.path.isfile(file_name), "{file_name} is not a valid file"
    df = pd.read_excel(file_name, skiprows=[0, 1])
    df.fillna(0, inplace=True)
    df.replace('X', 1, inplace=True)
    # First two columns are family name and type
    behaviors = df.values[:, 2:].astype(np.bool)
    __validate_extracted_behaviors__(behaviors)
    behaviors_list = []
    for behavior_array in behaviors:
        behaviors_list.append(list(behavior_array))
    file_details = pd.DataFrame()
    file_details['family_name'] = df['Malware Family'].str.lower().values
    file_details['behaviors'] = behaviors_list
    return file_details


def gather_familyname_behaviors_new(file_name):
    """
    Helper to read our updated behavioral annotations
    Parameters
    ----------
    file_name: str
        Input file name that pairs families to behaviors
    """
    assert os.path.isfile(file_name), "{file_name} is not a valid file"
    df = pd.read_csv(file_name)
    df.fillna(0, inplace=True)
    df.replace('x', 1, inplace=True)
    # First columns is behavior name
    behaviors = df.values[:, 1:].astype(np.bool)
    __validate_extracted_behaviors__(behaviors)
    behaviors_list = []
    for behavior_array in np.transpose(behaviors):
        behaviors_list.append(list(behavior_array))
    file_details = pd.DataFrame()
    family_names = list(df)[1:]
    family_names = [family_name.lower() for family_name in family_names]
    file_details['family_name'] = family_names
    file_details['behaviors'] = behaviors_list
    return file_details


def __validate_extracted_behaviors__(behaviors):
    behaviors = np.array(behaviors)
    all_values_real = np.all(np.isreal(behaviors))
    num_behaviors_0 = np.sum(behaviors == 0)
    num_behaviors_1 = np.sum(behaviors == 1)
    all_values_0_or_1 = (num_behaviors_0 + num_behaviors_1) == behaviors.size
    assert all_values_real, "Replacement of values failed in" + \
        "reading {file_name} there is a non-numeric value after parsing"
    assert all_values_0_or_1, "Replacement of values failed in" + \
        "reading {file_name} there are non-boolean values after parsing"


def gather_familyname_familynumber(file_name):
    """
    Helper to get class names from labels. Will pair with behaviors

    Parameters
    ----------
    file_name: str
        Input file name that pairs files to families
    """
    assert os.path.isfile(file_name), \
        "{} is not a valid file".format(file_name)
    df = pd.read_csv(file_name, header=0, delimiter=',')
    file_details = pd.DataFrame()
    file_details['family_name'] = df.Name
    file_details['family_number'] = df.Class
    return file_details


def gather_filename_familynumber(file_name):
    """
    Helper to get class labels. Will pair with behaviors
    Parameters
    ----------
    file_name: str
        Input file name that pairs files to families
    """
    assert os.path.isfile(file_name), "{} is not a valid file".format(file_name)
    df = pd.read_csv(file_name, header=0, delimiter=',')
    filenames = df.Id.values
    families = df.Class.values
    file_details = pd.DataFrame()
    file_details['filename'] = filenames
    file_details['family_number'] = families
    return file_details


def generate_image(file_name, shape, drop_column=0, output_dir=None,
                   save=False, elements_per_line=16):
    """
    Create Binary image(s) from file with Hex String
    Will truncate files that don't dived evenly into images.

    Parameters
    ----------
    file_name: str
        Input bytes file name
    shape: tuple(int,int)
        Shape of output image
    drop_column: int
        Column in bytes file to remove
    output_dir: str
        Where to save greyscale images
    elements_per_line: int
        Determines how many lines need to be read to satisfy shape
    save: bool
        Whether to save image to disk

    Returns
    -------
    image: PIL.image
        The first image from the binary file
    """
    assert os.path.isfile(file_name), \
        "{} is not a valid file".format(file_name)
    if save:
        assert os.path.isdir(output_dir), \
            "{} is not a valid file".format(output_dir)
    base_file = os.path.basename(file_name)
    base_file = base_file.rstrip('.bytes')
    if output_dir is None:
        output_dir = os.path.dirname(file_name)

    # Read and clean data 
    number_of_rows = (np.prod(shape) // elements_per_line) + 1
    bytes_data = get_bytes_from_file(file_name, number_of_rows, drop_column)

    # Numpy is row,col where PIL is width,height
    output_size = int(np.prod(shape))
    input_size = int(len(bytes_data))
    num_images = int(input_size/output_size)
    assert output_size <= input_size, "Binary size of {} is too small for output image of {}".format(input_size, shape)

    # Truncate files that don't divide into integer number of images 
    if input_size % output_size != 0:
        input_size = int(output_size*np.floor(input_size/output_size))
        num_images = int(input_size/output_size)
        bytes_data = bytes_data[0:input_size]
    if save:
        print("Total size of Binary is {}, which yields {} {} images"
              .format(input_size, num_images, output_size))

        # Print images
        for i in range(num_images):
            # L for 8 bit binary, shape of image, data slice
            image_data = Image.frombytes('L', shape,
                                         bytes_data[i*output_size:(i+1)*output_size])
            output_file = os.path.join(output_dir, "{}_{}.png"
                                       .format(base_file, i))
            image_data.save(output_file)
    return Image.frombytes('L', shape, bytes_data[:output_size])


def get_bytes_from_file(file_name, number_of_rows, drop_column=0):
    df = pd.read_csv(file_name, header=None, delimiter=' ',
                     dtype=str, na_values=['NaN', '??'], nrows=number_of_rows)
    df.drop(columns=drop_column, inplace=True)
    df.fillna('00', inplace=True)
    string_data = df.values
    return convert_string_to_bytes(string_data)


def convert_string_to_bytes(data):
    hex_converter = np.vectorize(bytes.fromhex)
    return hex_converter(data).tobytes()


if __name__ == "__main__":
    print("Generating sample image from bytes")
    output_path = ""
    if not os.path.isdir(output_path):
        os.mkdir(output_path)
    file_path = "" # Path to specific bytes files
    generate_image(file_path, (224, 224), output_dir=output_path)
