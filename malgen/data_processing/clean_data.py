################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

import numpy as np
from malgen.data_processing.gather_data import get_bytes_from_file
import glob
import os
import warnings
from multiprocessing import Pool
from multiprocessing import cpu_count
import time


def file_to_nparray(bytes_file, output_dir=None):
    output_dir, filename = determine_output_dir(bytes_file, output_dir)
    bytes = get_bytes_from_file(bytes_file, number_of_rows=None)
    np_array = np.frombuffer(bytes, dtype=np.uint8)
    write_nparray(np_array, filename, output_dir)


def determine_output_dir(bytes_file, output_dir=None):
    path, filename = os.path.split(bytes_file)
    if output_dir is None:
        parent_path = os.path.dirname(path)
        new_dir = "extracted_bytes_{}".format(os.path.basename(path))
        output_dir = os.path.join(parent_path, new_dir)
    return output_dir, filename


def write_nparray(np_array, bytes_file, output_dir):
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    output_path = os.path.join(output_dir, bytes_file+".npy")

    if os.path.isfile(output_path):
        warnings.warn("File: {} already exists. Skipping...".
                      format(output_path))
    else:
        np.save(output_path, np_array, allow_pickle=False)


def all_files_to_nparray(list_files, output_dir=None):
    with Pool(cpu_count() - 20) as p:
        p.map(file_to_nparray, list_files)


if __name__ == '__main__':
    start = time.time()
    path_to_training_data = "" #<base>/train containing kaggle bytes
    path = os.path.join(path_to_training_data, "*.bytes")
    all_files = glob.glob(path)
    all_files_to_nparray(all_files)
    print("Took {}s to clean all files".format(time.time()-start))
