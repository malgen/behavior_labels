################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

from multiprocessing import Pool
from multiprocessing import cpu_count
import random
import numpy as np
import time
import glob
from itertools import repeat

# Our TF model was CPU bottlenecked and we had enough memory 
# to just load all the data at once

def get_padded_file(np_file, maxlen, padding_char):
    np_array = np.load(np_file)
    amount_to_pad = (0,max(0,maxlen-len(np_array)))
    np_array = np.pad(np_array[0:maxlen],amount_to_pad,'constant',\
        constant_values=padding_char)
    return np_array

def get_all_files(files, maxlen, padding_char):
    pool = Pool(cpu_count()-20)
    data = pool.starmap(get_padded_file, zip(files, \
                                       repeat(maxlen),\
                                       repeat(padding_char)))
    return data

if __name__=='__main__':
    start = time.time()
    path_to_npy_file = "" #<hash>.bytes.npy
    all_files = glob.glob(path_to_npy_file)
    loaded_files = get_all_files(all_files, 2**20, 257)
    print("Took {}s to load all files".format(time.time()-start))
    print("Len: {}".format(len(loaded_files)))
    idx = random.randint(0, len(loaded_files))
    first = loaded_files[idx]
    first_raw = get_padded_file(all_files[idx],2**20,257)
    print("Parallel load matches serial: {}"/format(np.all(first==first_raw)))
