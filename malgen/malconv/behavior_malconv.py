#!/usr/bin/python

################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

'''defines the MalConv architecture.
https://github.com/endgameinc/ember/blob/master/malconv/malconv.py
Which is adapted from https://arxiv.org/pdf/1710.09435.pdf
Things different this implementation and that of the original paper by Raff et al:
 * The paper uses batch_size = 256 and SGD(lr=0.01, momentum=0.9, decay=UNDISCLOSED, nesterov=True )
 * The paper didn't have a special EOF symbol
 * The paper allowed for up to 2MB malware sizes, we use 1.0MB 
 '''
import tensorflow as tf
from tensorflow.keras.layers import Dense, Conv1D, Activation, GlobalMaxPooling1D, Input, Embedding, Multiply
from tensorflow.keras.models import Model
from tensorflow.keras import backend as K
from tensorflow.keras import metrics
from tensorflow.keras.callbacks import LearningRateScheduler
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import SGD
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import math, random, argparse, os, requests, glob
from random import shuffle
from malgen.data_processing.tf_data_pipeline import get_all_files

from malgen.data_processing.gather_data import\
    gather_all_parts

SEED = 123
BYTES_FILES = glob.glob("/path_to/extracted_bytes_train/*.npy")
FAMILY_FILE = "/path_to/trainLabels.csv"
BEHAVIOR_FILE = "/path_to/microsoftdata_behaviors.xlsx"
FAMILY_NAME_FILE = "/path_to/labelsToClassName.csv"
PRETRAIN_MODEL_LOCATION = "" #.h5 from ember

def CreateMalconv(model_location, number_of_behaviors, input_dim):
    if os.path.exists(model_location):
        print("restoring malconv.h5 from disk for continuation training...")
        pretrained_model = load_model(model_location)
        _, maxlen, embedding_size = pretrained_model.layers[1].output_shape
        behavior_output = Dense(number_of_behaviors, activation='sigmoid', name='dense_2')(pretrained_model.get_layer('dense_1').output)
        basemodel = Model(inputs=pretrained_model.input, outputs = behavior_output)
        input_dim
    else: # We did transfer learning so this branch is unexplored
        maxlen = 2**20 # 1MB
        embedding_size = 8
        # define model structure
        inp = Input( shape=(maxlen,))
        emb = Embedding( input_dim, embedding_size )( inp )
        filt = Conv1D( filters=128, kernel_size=500, strides=500, use_bias=True, activation='relu', padding='valid' )(emb)
        attn = Conv1D( filters=128, kernel_size=500, strides=500, use_bias=True, activation='sigmoid', padding='valid')(emb)
        gated = Multiply()([filt,attn])
        feat = GlobalMaxPooling1D()( gated )
        dense = Dense(128, activation='relu')(feat)
        outp = Dense(number_of_behaviors, activation='sigmoid')(dense)
        basemodel = Model( inp, outp )
    return basemodel, maxlen

def train(hold_out_family="gatak", gpu_num=0):
    os.environ["CUDA_VISIBLE_DEVICES"]="{}".format(gpu_num)
    model_location = PRETRAIN_MODEL_LOCATION
    batch_size = 128
    input_dim = 257 # every byte plus a special padding symbol
    padding_char = 256
    number_of_behaviors = 56

    basemodel,maxlen = CreateMalconv(model_location, number_of_behaviors, input_dim)
    basemodel.summary()
    model = basemodel
    model.compile( loss='binary_crossentropy', optimizer=SGD(learning_rate=0.01,momentum=0.9,nesterov=True,decay=1e-3), metrics=[metrics.binary_accuracy] )

    data_items = gather_all_parts(BYTES_FILES,FAMILY_FILE,\
                   FAMILY_NAME_FILE, BEHAVIOR_FILE)
    data_items = data_items[ data_items['behaviors'] != -1 ] # get only labeled samples
    if hold_out_family is None:
        hold_out_family = "allFamilies"
        np.random.seed(SEED)
        mask = np.random.rand(len(data_items)) < 0.8
        train_items = data_items[mask]
        test_items = data_items[~mask]
    else:
        test_items = data_items[data_items['family_name']==hold_out_family]
        train_items = data_items[~(data_items['family_name']==hold_out_family)]
    train_labels = train_items['behaviors'].tolist()
    train_hashes = train_items['file_path'].tolist()
    test_labels = test_items['behaviors'].tolist()
    test_hashes = test_items['file_path'].tolist()

    print("Loading Data")
    train_examples =  np.array(get_all_files(train_hashes, maxlen, padding_char))
    test_examples =  np.array(get_all_files(test_hashes, maxlen, padding_char))
    print("Done Loading")
    train_gen = tf.data.Dataset.from_tensor_slices((train_examples, train_labels)).\
            shuffle(11000, reshuffle_each_iteration=True).batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)
    test_gen = tf.data.Dataset.from_tensor_slices((test_examples, test_labels))


    base = K.get_value( model.optimizer.lr )
    def schedule(epoch):
        return base / 10.0**(epoch//2)
    print("Begin Training")
    model.fit(
        train_gen,
        #epochs=10,
        epochs=1,
        callbacks=[ LearningRateScheduler( schedule ) ],
    )
    basemodel.save('{}_behavior_malconv.h5'.format(hold_out_family))

def test(hold_out_family="gatak"):
    os.environ["CUDA_VISIBLE_DEVICES"]="3"
    if hold_out_family is None:
        hold_out_family = "allFamilies"
    model_location = '{}_behavior_malconv.h5'.format(hold_out_family)
    batch_size = 128
    input_dim = 257 # every byte plus a special padding symbol
    padding_char = 256
    number_of_behaviors = 56

    pretrained_model = load_model(model_location)
    _, maxlen, embedding_size = pretrained_model.layers[1].output_shape
    model= pretrained_model
    model.compile( loss='binary_crossentropy', optimizer=SGD(learning_rate=0.01,momentum=0.9,nesterov=True,decay=1e-3), metrics=[metrics.binary_accuracy] )

    data_items = gather_all_parts(BYTES_FILES,FAMILY_FILE,\
                   FAMILY_NAME_FILE, BEHAVIOR_FILE)
    data_items = data_items[ data_items['behaviors'] != -1 ] # get only labeled samples
    if hold_out_family == "allFamilies":
        np.random.seed(SEED)
        mask = np.random.rand(len(data_items)) < 0.8
        test_items = data_items[~mask]
    else:
        test_items = data_items[data_items['family_name']==hold_out_family]
    test_labels = test_items['behaviors'].tolist()
    test_hashes = test_items['file_path'].tolist()

    test_examples =  np.array(get_all_files(test_hashes, maxlen, padding_char))
    test_gen = tf.data.Dataset.from_tensor_slices((test_examples, test_labels)).batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)
    print("Done Loading")
    test_p = model.predict(test_gen,
                               verbose=1 )
    np.savetxt("{}BehaviorPredictions.csv".format(hold_out_family), test_p, delimiter=",")

if __name__ == '__main__':
    train()
    test()
