################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

from malgen.malconv.behavior_malconv import train, test,\
    BYTES_FILES, FAMILY_FILE, FAMILY_NAME_FILE, BEHAVIOR_FILE
from malgen.data_processing.gather_data import \
    gather_familyname_behaviors, gather_all_parts
import pandas as pd
import numpy as np
import time

def train_models():
    df = gather_all_parts(BYTES_FILES,FAMILY_FILE,\
                   FAMILY_NAME_FILE, BEHAVIOR_FILE)
    families = pd.unique(df['family_name'])
    for family in families:
        start_train = time.time()
        train(family)
        end_train = time.time()
        print("{} took {}s to train".format(family, end_train-start_train))
        test(family)
        end_test = time.time()
        print("{} took {}s to test".format(family, end_test-end_train))
    start_train = time.time()
    train(None)
    end_train = time.time()
    print("{} took {}s to train".format("All Families", end_train-start_train))
    test(None)
    end_test = time.time()
    print("{} took {}s to test".format("All Families", end_test-end_train))

def evaluate_results():
    ground_truth = gather_familyname_behaviors(BEHAVIOR_FILE)
    families = ground_truth['family_name'].values
    performance = pd.DataFrame()
    for family in families:
        df_pred = pd.read_csv("{}BehaviorPredictions.csv".format(family),\
            header=None)
        np_pred = df_pred.values
        np_pred_round = np.round(np_pred).astype(np.bool)
        family_idx = ground_truth['family_name']==family
        family_ground_truth = ground_truth[family_idx].behaviors.values[0]
        correct = (family_ground_truth==np_pred_round)
        total_predictions = np_pred_round.size
        total_behavior_predictions = np_pred_round.shape[0]
        correct_by_behavior = np.sum(correct,axis=0)
        print("{}:\n\tTotal Accuracy:{}".format(\
                family, np.sum(correct)/total_predictions))
        performance[family] = correct_by_behavior/total_behavior_predictions
    performance.to_csv("BehaviorAccuracyPerFamily.csv",index=False)

if __name__=='__main__':
    train_models()
    evaluate_results()
