# Malgen Malconv Transfer Learning

## Modifications
We mostly adapted the original model definition to work with our current version of packages and
with our transfer learning definition.

## License Details From Original Model
"Source code in this repository is covered by the GNU Affero General Public
License version 3 (AGPL-v3)."

## Side Note about CudNN error
EBMER authors had issues with cudNN loading.
On linux setting `export TF_FORCE_GPU_ALLOW_GROWTH=true` resolved my issue.
See this [issue](https://github.com/tensorflow/tensorflow/issues/24496) for more details.

# Details from original code
For more details about MalConv, please see (and cite) the [original paper](https://arxiv.org/abs/1710.09435).

```
Raff, Edward, et al. "Malware detection by eating a whole exe." arXiv preprint arXiv:1710.09435 (2017).
```

If you use the pre-trained weights or code in your work, please cite [the EMBER paper](https://arxiv.org/pdf/1804.04637.pdf) for the implementation of MalConv, as it differs in a few subtle ways from the original.

```
H. Anderson and P. Roth, "EMBER: An Open Dataset for Training Static PE Malware Machine Learning Models”, in ArXiv e-prints. Apr. 2018.

@ARTICLE{2018arXiv180404637A,
  author = {{Anderson}, H.~S. and {Roth}, P.},
  title = "{EMBER: An Open Dataset for Training Static PE Malware Machine Learning Models}",
  journal = {ArXiv e-prints},
  archivePrefix = "arXiv",
  eprint = {1804.04637},
  primaryClass = "cs.CR",
  keywords = {Computer Science - Cryptography and Security},
  year = 2018,
  month = apr,
  adsurl = {http://adsabs.harvard.edu/abs/2018arXiv180404637A},
}
```

## How does the EMBER MalConv model differ from that of Raff et al.?
 * The EBMER model was trained on binary files from labeled samples in the EMBER training set.
 * The original paper used `batch_size = 256` and `SGD(lr=0.01, momentum=0.9, decay=UNDISCLOSED, nesterov=True )`.  EMBER used
 `decay=1e-3` and `batch_size=100`.
 * It is unknown whether the original paper used a special symbol for padding.
 * The paper allowed for up to 2MB malware sizes, EMBER uses 1MB because of memory limits on a commonly-used Titan X.

## How does the MalGen model differ from EMBER?
* We use the [pretrained malconv model trained on the EMBER data set](https://github.com/endgameinc/ember/tree/master/malconv) 
and modify the architecture to predict multiple behaviors in an executable
* The model is further fine-tuned using the Microsoft Malware data and our behavior labels.
