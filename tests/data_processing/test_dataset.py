################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

import pytest
from malgen.data_processing.pytorch_data_pipeline import \
    MicrosoftMalwareImages,MicrosoftMalwareFamilyImages, MicrosoftMalwareBehaviorImages
import glob
import torch
import numpy as np
import pandas as pd
from mock_microsoftMW_data import IMG_SHAPE
from mock_microsoftMW_data import *

@pytest.fixture
def create_base_class(create_mock_files):
    mock_files = create_mock_files
    dataset = MicrosoftMalwareImages(*mock_files, IMG_SHAPE)
    return dataset

@pytest.fixture
def create_family_class(create_mock_files):
    mock_files = create_mock_files
    dataset = MicrosoftMalwareFamilyImages(*mock_files, IMG_SHAPE)
    return dataset

@pytest.fixture
def create_behavior_class(create_mock_files):
    mock_files = create_mock_files
    dataset = MicrosoftMalwareBehaviorImages(*mock_files, IMG_SHAPE)
    return dataset

@pytest.fixture
def create_family_binary_class(create_mock_files):
    mock_files = create_mock_files
    dataset = MicrosoftMalwareFamilyImages(*mock_files, IMG_SHAPE, True)
    return dataset

def test_init(create_base_class):
    dataset = create_base_class
    assert True # Smoke test

def test_len(create_base_class, create_mock_files):
    mock_files = create_mock_files
    FAMILY_FILE = mock_files[1]
    dataset = create_base_class
    df = pd.read_csv(FAMILY_FILE)
    family_idx = df['Class']!=8
    assert len(dataset) == np.sum(family_idx)

def test_getitem(create_base_class):
    dataset = create_base_class
    with pytest.raises(NotImplementedError):
        image, label = dataset[0,1]

def test_get_image(create_base_class):
    dataset = create_base_class
    image, image_filename = dataset.__get_image__(0,1)
    image, image_filename = dataset.__get_image__(0)

def test_getitem_single_family(create_family_class):
    dataset = create_family_class
    index = 0
    family = 1
    image, label = dataset[index, family]
    tensor_size_height_width = tuple(torch.squeeze(image).size())
    tensor_size_width_height = tuple(reversed(tensor_size_height_width))
    assert tensor_size_width_height == IMG_SHAPE
    assert label == family-1

def test_getitem_multi_family(create_family_class):
    dataset = create_family_class
    dataset.batched = False
    index = 0
    image, label = dataset[index]
    tensor_size_height_width = tuple(torch.squeeze(image).size())
    tensor_size_width_height = tuple(reversed(tensor_size_height_width))
    assert tensor_size_width_height == IMG_SHAPE
    assert label == 0

def test_getitem_binary_family(create_family_binary_class):
    dataset = create_family_binary_class
    index = 0
    family = 1
    image, label = dataset[index, family]
    tensor_size_height_width = tuple(torch.squeeze(image).size())
    tensor_size_width_height = tuple(reversed(tensor_size_height_width))
    assert tensor_size_width_height == IMG_SHAPE
    family_idx = np.where(label)[0]
    assert family_idx == family -1

def test_getitem_behavior(create_behavior_class):
    dataset = create_behavior_class
    index = 0
    family = 1
    image, label = dataset[index, family]
    tensor_size_height_width = tuple(torch.squeeze(image).size())
    tensor_size_width_height = tuple(reversed(tensor_size_height_width))
    assert tensor_size_width_height == IMG_SHAPE
    assert len(label) > 1
