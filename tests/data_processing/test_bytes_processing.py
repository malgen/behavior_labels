################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

from malgen.data_processing.gather_data import\
    gather_filepath_filename, gather_familyname_familynumber,\
    __validate_extracted_behaviors__,gather_familyname_behaviors,\
    gather_filename_familynumber, gather_all_parts,\
    gather_familyname_behaviors_new
import pandas as pd
import pytest
import numpy as np
import os

# Mock Files
@pytest.fixture()
def filepath_and_filenames():
    sample_file_paths = ['~/cats.txt', '~/dogs.txt']
    sample_file_names = ['cats','dogs']
    return sample_file_paths, sample_file_names

@pytest.fixture()
def path_familyname_familynumber(tmp_path):
    d = tmp_path
    tmp_file = "family_name_file.csv"
    p = d/ tmp_file
    family_name = "gatak"
    family_number = 1
    p.write_text("Name,Class\n{},{}".format(family_name,family_number))
    file_path = os.path.join(tmp_path, tmp_file)
    return file_path,family_name,family_number

@pytest.fixture()
def path_familyname_behaviors(tmp_path):
    d = tmp_path
    tmp_file = "behaviors_file.xlsx"
    p = d/ tmp_file
    family = "gatak".upper()
    behaviors = [0, 1]
    df = pd.DataFrame(\
                    {"Malware Family":{0:family}, \
                    "Type":{0:"type"},\
                    "Behavior 1":{0:behaviors[0]},\
                    "Behavior 2":{0:behaviors[1]}})
    df.to_excel(p, index=False, startrow=2)
    file_path = os.path.join(tmp_path, tmp_file)
    return file_path, family, behaviors

@pytest.fixture()
def path_familyname_behaviors_new(tmp_path):
    d = tmp_path
    tmp_file = "behaviors_file.csv"
    p = d/ tmp_file
    family = "gatak".upper()
    behaviors = [0, 1]
    df = pd.DataFrame(\
                    {"Behaviors":{0:"B1",1:"B2"}, \
                    family:{0:behaviors[0],1:behaviors[1]}})
    df.to_csv(p, index=False)
    file_path = os.path.join(tmp_path, tmp_file)
    return file_path, family, behaviors

@pytest.fixture()
def path_filename_familynumber(tmp_path):
    d = tmp_path
    tmp_file = "family_file.csv"
    p = d/ tmp_file
    file_path = "cats"
    familynumber = 1
    p.write_text("Id,Class\n{},{}".format(file_path,familynumber))
    sample_path = os.path.join(tmp_path, tmp_file)
    return sample_path, file_path, familynumber

@pytest.fixture()
def all_fixtures(filepath_and_filenames, path_familyname_familynumber,\
    path_familyname_behaviors, path_filename_familynumber):
    bytes_file, name = filepath_and_filenames
    family_name_file, name_2, number = path_familyname_familynumber
    behavior_file, name_3, behaviors = \
        path_familyname_behaviors
    family_file, path, number_2 =\
        path_filename_familynumber
    return bytes_file, name, family_name_file, name_2, number,\
        behavior_file, name_3, behaviors, family_file, path, number_2


# Tests
def test_file_path_filename(filepath_and_filenames):
    file_paths, filenames = filepath_and_filenames
    file_details = gather_filepath_filename(file_paths)
    assert list(file_details) == ['file_path','filename']
    assert all(file_details['filename'].values == filenames)

def test_familyname_familynumber(path_familyname_familynumber):
    file_path, name, number = path_familyname_familynumber
    file_details = gather_familyname_familynumber(file_path)
    assert list(file_details) == ['family_name','family_number']
    assert file_details['family_name'].values ==  name
    assert file_details['family_number'].values == number

def test_validate_extracted_behaviors():
    valid_behaviors = np.ones((2,2))
    __validate_extracted_behaviors__(valid_behaviors)

    with pytest.raises(AssertionError):
        non_numeric_value = np.array([['a',1],[1,1]])
        __validate_extracted_behaviors__(non_numeric_value)

    with pytest.raises(AssertionError):
        non_bool_value = valid_behaviors.copy()
        non_bool_value[0] = 30
        __validate_extracted_behaviors__(non_bool_value)

def test_familyname_behaviors(path_familyname_behaviors):
    file_path, family_name, behaviors = \
        path_familyname_behaviors
    file_details = gather_familyname_behaviors(file_path)
    assert list(file_details) == ['family_name', 'behaviors']
    assert file_details['family_name'].values == family_name.lower()
    assert file_details['behaviors'].values[0] == behaviors

def test_familyname_behaviors_new(path_familyname_behaviors_new):
    file_path, family_name, behaviors = \
        path_familyname_behaviors_new
    file_details = gather_familyname_behaviors_new(file_path)
    assert list(file_details) == ['family_name', 'behaviors']
    assert file_details['family_name'].values == family_name.lower()
    assert file_details['behaviors'].values[0] == behaviors

def test_filename_familynumber(path_filename_familynumber):
    path, filename, family_number =\
        path_filename_familynumber
    file_details = gather_filename_familynumber(path)
    assert list(file_details) == ['filename','family_number']
    assert file_details['filename'].values == filename
    assert file_details['family_number'].values == family_number

def test_gather_all(all_fixtures):
    bytes_file, name, family_name_file, name_2, number,\
        behavior_file, name_3, behaviors, family_file, path, number_2 = \
        all_fixtures
    file_details = gather_all_parts(bytes_file, family_file, family_name_file, behavior_file)
    assert len(list(file_details))==5
