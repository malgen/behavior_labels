################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

import pytest
from malgen.data_processing.pytorch_data_pipeline import \
    MicrosoftMalwareImages, MalwareBatchSampler
import glob
import torch
from test_dataset import create_base_class
from mock_microsoftMW_data import *

NUM_SAMPLES = 5

def test_init(create_base_class):
    dataset = create_base_class
    batch_sampler = MalwareBatchSampler(dataset, NUM_SAMPLES)
    assert True # Smoke test

def test_len(create_base_class):
    dataset = create_base_class
    batch_sampler = MalwareBatchSampler(dataset, NUM_SAMPLES)
    assert len(batch_sampler) == NUM_SAMPLES

def test_iter(create_base_class):
    dataset = create_base_class
    batch_sampler = MalwareBatchSampler(dataset, NUM_SAMPLES)
    batch_iterator = iter(batch_sampler)
    indices = next(batch_iterator)
    # TODO: Currently our base dataset only has one sample per family
    assert len(indices) == 1 #NUM_SAMPLES
