################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

import glob
import numpy as np
import pandas as pd
import os
import tempfile
import pytest

IMG_SHAPE=(224,224)

@pytest.fixture()
def create_family_file():
    tmp_path = tempfile.gettempdir()
    d = tmp_path
    tmp_file = "trainLabels.csv"
    p = os.path.join(d, tmp_file)
    contents = "\"Id\",\"Class\"\n"
    for i in [1,2,3,4,5,6,7,9]:
        contents+="\"bytes_{}\",{}\n".format(i,i)
    with open(p, 'w') as f:
        f.write(contents)
    return p

@pytest.fixture()
def create_family_name_file():
    tmp_path = tempfile.gettempdir()
    d = tmp_path
    tmp_file = "labels_to_classname.csv"
    p = os.path.join(d, tmp_file)
    contents = "\"Class\",\"Name\"\n1,\"ramnit\"\n2,\"lollipop\"\n"+\
        "3,\"kelihos\"\n4,\"vundo\"\n5,\"simda\"\n6,\"tracur\"\n7,\"kelihos\"\n"+\
        "8,\"obfuscator.acy\"\n9,\"gatak\""
    with open(p,'w') as f:
        f.write(contents)
    return p

@pytest.fixture()
def create_behavior_file():
    tmp_path = tempfile.gettempdir()
    d = tmp_path
    tmp_file = "behaviors_file.xlsx"
    p = os.path.join(d, tmp_file)
    families = ["gatak", "kelihos", "vundo", "simda",
            "tracur", "ramnit", "lollipop"]
    families = [family.upper() for family in families]
    families = {i:family for i,family in enumerate(families)}
    types = {i:"type" for i in range(len(families))}
    behavior_1 = {i:0 for i in range(len(families))}
    behavior_2 = {i:1 for i in range(len(families))}
    df = pd.DataFrame(\
                    {"Malware Family":families, \
                    "Type": types,\
                    "Behavior 1": behavior_1,\
                    "Behavior 2": behavior_2})
    df.to_excel(p, index=False, startrow=2)
    return p

@pytest.fixture()
def create_bytes_files():
    list_files = []
    tmp_path = tempfile.gettempdir()
    d = tmp_path
    for i in [1,2,3,4,5,6,7,9]:
        tmp_file = "bytes_{}.bytes".format(i)
        p = os.path.join(d, tmp_file)
        line_padding = "0"*8
        contents = line_padding
        line_width = 16
        for j in range(line_width):
            contents += " {}{}".format(i,i)
        contents += "\n"
        num_lines = np.prod(IMG_SHAPE)//line_width + 1
        with open(p,'w') as f:
            f.write(contents*num_lines)
        list_files.append(p)
    return list_files

@pytest.fixture()
def create_mock_files(create_family_file,create_family_name_file,\
        create_behavior_file, create_bytes_files):
    BYTES_FILES = create_bytes_files
    FAMILY_FILE = create_family_file
    FAMILY_NAME_FILE = create_family_name_file
    BEHAVIOR_FILE = create_behavior_file
    return [BYTES_FILES, FAMILY_FILE, FAMILY_NAME_FILE, BEHAVIOR_FILE]

def test_fixtures(create_mock_files):
    mock_files = create_mock_files
    return True
