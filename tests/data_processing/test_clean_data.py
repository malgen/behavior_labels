################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

import pytest
from malgen.data_processing.clean_data import\
    determine_output_dir, write_nparray
import tempfile
import random
import string
import os
import numpy as np

@pytest.fixture()
def create_bytes_file():
    tmp_dir = tempfile.gettempdir()
    random_filename = ''.join(random.choices(string.ascii_uppercase, k=4))
    tmp_file = "{}.bytes".format(random_filename)
    bytes_path = os.path.join(tmp_dir, tmp_file)
    line_padding = "0"*8
    contents = line_padding
    line_width = 16
    for j in range(line_width):
        contents += " {}{}".format(0,0)
    contents += "\n"
    num_lines = 4000
    with open(bytes_path,'w') as f:
        f.write(contents*num_lines)
    return bytes_path

def test_determine_output_dir(create_bytes_file):
    bytes_path = create_bytes_file
    desired_output_dir = tempfile.gettempdir()
    output_dir, filename = determine_output_dir(bytes_path, desired_output_dir)
    assert output_dir==desired_output_dir
    assert filename in bytes_path

def test_write_array(create_bytes_file):
    bytes_path = create_bytes_file
    output_dir = tempfile.gettempdir()
    random_subdir = ''.join(random.choices(string.ascii_uppercase, k=4))
    output_dir = os.path.join(output_dir,random_subdir)
    output_dir, filename = determine_output_dir(bytes_path, output_dir)
    save_data = np.zeros((5,0))
    write_nparray(save_data, filename, output_dir)
    save_file = os.path.join(output_dir,filename+".npy")
    assert os.path.isdir(output_dir)
    assert os.path.isfile(save_file)
    load_data = np.load(save_file)
    assert np.all(load_data==save_data)
    os.remove(save_file)
