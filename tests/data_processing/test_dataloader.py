################################################################################
# MalGen
#
# Copyright 2020 National Technology & Engineering Solutions of Sandia, LLC 
# (NTESS). Under the terms of Contract DE-NA0003525 with NTESS, the U.S. 
# Government retains certain rights in this software.
# 
# This software is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE V3
################################################################################

import pytest
from malgen.data_processing.pytorch_data_pipeline import \
    MicrosoftMalwareImages,MicrosoftMalwareFamilyImages, MicrosoftMalwareBehaviorImages
from malgen.data_processing.pytorch_data_pipeline import \
    MalwareBatchSampler
import glob
import torch
import time
import random
from test_dataset import create_family_class
from mock_microsoftMW_data import *

NUM_SAMPLES = 5
SUBSET = NUM_SAMPLES*5


def test_iter(create_family_class):
    dataset = create_family_class
    batch_sampler = MalwareBatchSampler(dataset, NUM_SAMPLES)
    my_data = torch.utils.data.DataLoader(dataset,batch_sampler=batch_sampler)
    number_batches = 0
    for batch in my_data:
        number_batches += 1
    assert True

def test_multiple_epochs_consistent_samples(create_family_class):
    dataset = create_family_class
    batch_sampler = MalwareBatchSampler(dataset, NUM_SAMPLES)
    my_data = torch.utils.data.DataLoader(dataset,batch_sampler=batch_sampler)
    samples_in_epoch = []
    for epoch in range(2):
        samples_in_batches = 0
        for batch in my_data:
            samples_in_batches += len(batch)
        samples_in_epoch.append(samples_in_batches)
    assert samples_in_epoch.count(samples_in_epoch[0]) == \
            len(samples_in_epoch)

def test_parallel(create_family_class):
    dataset = create_family_class
    batch_sampler = MalwareBatchSampler(dataset, NUM_SAMPLES)
    my_data = torch.utils.data.DataLoader(dataset,batch_sampler=batch_sampler)
    number_batches = 0
    for batch in my_data:
        number_batches += 1

    my_data = torch.utils.data.DataLoader(dataset,batch_sampler=batch_sampler,\
                                            num_workers = 2)
    parallel_number_batches = 0
    for batch in my_data:
        parallel_number_batches += 1
    # Timing variability was dreadful for small batch sizes 
    # so just verify got the same results
    assert parallel_number_batches == number_batches 
